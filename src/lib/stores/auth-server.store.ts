import type { Login } from '../domain/Login';
import requestServer from '../utils/requestServer';
import {browser} from "$app/environment";

type JwtToken = {
	id_token: string;
};

function authenticateSuccess(response: JwtToken, rememberMe: boolean): void {
	const jwt = response.id_token;
	if (rememberMe) {
		localStorage.setItem('authenticationToken', jwt);
		sessionStorage.removeItem('authenticationToken');
	} else {
		sessionStorage.setItem('authenticationToken', jwt);
		localStorage.removeItem('authenticationToken');
	}
}

export const authServerProvider = {

	getToken(): string {
		if (!browser) {
			return null;
		}
		const tokenInLocalStorage: string | null = localStorage.getItem('authenticationToken');
		const tokenInSessionStorage: string | null = sessionStorage.getItem('authenticationToken');
		return tokenInLocalStorage ?? tokenInSessionStorage ?? '';
	},
	login(credentials: Login): Promise<void> {
		return requestServer('http://localhost:8080/api/authenticate',
			{
				method: 'POST',
				body: JSON.stringify(credentials),
				headers: {
					'Content-Type': 'application/json'
				}
			}
		)
			.then(response => response.json())
			.then(response => {
				authenticateSuccess(response, credentials.rememberMe);
			});
	},
	logout(): Promise<void> {
		return new Promise(() => {
			localStorage.removeItem('authenticationToken');
			sessionStorage.removeItem('authenticationToken');
		});
	}
};
