import { writable } from 'svelte/store';
import type { Account } from '../domain/Account';
import requestServer from '../utils/requestServer';

const { set, subscribe } = writable<Account | null>(null);

let currentAccount: Account | null = null;

function updateAccount(newAccount: Account | null): void {
	currentAccount = newAccount;
	set(newAccount);
}

async function fetchAccount(): Promise<void> {
	requestServer('http://localhost:8080/api/account')
		.then(response => {
			if (response.status === 200) {
				return response.json();
			}
			return null;
		})
		.then((fetchedAccount: Account) => updateAccount(fetchedAccount));

}

fetchAccount();

export const accountStore = {
	subscribe,
	updateAccount,
	fireLogin: (): void => {
		fetchAccount().catch();
	},
	isAuthenticated: (): boolean => currentAccount !== null,
	hasAnyAuthority(authorities: string[] | string): boolean {
		if (!currentAccount) {
			return false;
		}
		if (!Array.isArray(authorities)) {
			authorities = [authorities];
		}
		return currentAccount.authorities.some((authority: string) => authorities.includes(authority));
	}

};