import { readable } from 'svelte/store';
import type { InfoResponse, ProfileInfo } from '../domain/ProfileInfo';
import requestServer from '../utils/requestServer';

export const profileStore = readable<ProfileInfo>(null, (set) => {
	async function getProfile() {
		const result = await requestServer('http://localhost:8080/management/info', {});

		const profile = await result.json().then((response: InfoResponse) => {
			const buildProfile: ProfileInfo = {
				activeProfiles: response.activeProfiles,
				inProduction: response.activeProfiles?.includes('prod'),
				openAPIEnabled: response.activeProfiles?.includes('api-docs')
			};
			if (response.activeProfiles && response['display-ribbon-on-profiles']) {
				const displayRibbonOnProfiles = response['display-ribbon-on-profiles'].split(',');
				const ribbonProfiles = displayRibbonOnProfiles.filter(availableProfile => response.activeProfiles?.includes(availableProfile));
				if (ribbonProfiles.length > 0) {
					buildProfile.ribbonEnv = ribbonProfiles[0];
				}
			}

			return buildProfile;
		});

		set(profile);
	}

	getProfile();
});