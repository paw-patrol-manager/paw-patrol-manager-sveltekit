import type { Login } from '../domain/Login';
import { authServerProvider } from './auth-server.store';
import { accountStore } from './account.store';

export const loginStore = {
	login(credentials: Login): Promise<void> {
		return authServerProvider.login(credentials).then(() => accountStore.fireLogin());
	},
	logout(): void {
		authServerProvider.logout().then(() => (() => this.accountService.authenticate(null)));
	}
};
