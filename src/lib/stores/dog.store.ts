import requestServer from '../utils/requestServer';
import type { IDog } from '../domain/Dog';
import { getDogIdentifier } from '../domain/Dog';
import { isPresent } from '../utils/operators';

const resourceUrl = 'http://localhost:8080/api/dogs';

export const dogStore = {

	create(dog: IDog): Promise<IDog> {
		return requestServer(resourceUrl, { method: 'POST', body: JSON.stringify(dog) })
			.then(response => response.json());
	},

	update(dog: IDog): Promise<IDog> {
		return requestServer(`${resourceUrl}/${getDogIdentifier(dog)}`, {
			body: JSON.stringify(dog),
			method: 'PUT'
		})
			.then(response => response.json());
	},

	partialUpdate(dog: IDog): Promise<IDog> {
		return requestServer(`${resourceUrl}/${getDogIdentifier(dog)}`, {
			body: JSON.stringify(dog),
			method: 'PATCH'
		})
			.then(response => response.json());
	},

	find(id: number): Promise<IDog> {
		return requestServer(`${resourceUrl}/${id}`)
			.then(response => response.json());
	},

	query(): Promise<IDog[]> {
		return requestServer(resourceUrl)
			.then(response => response.json());
	},

	deleteDog(id: number): Promise<Response> {
		return requestServer(`${resourceUrl}/${id}`, { method: 'DELETE' });
	},

	addDogToCollectionIfMissing(dogCollection: IDog[], ...dogsToCheck: (IDog | null | undefined)[]): IDog[] {
		const dogs: IDog[] = dogsToCheck.filter(isPresent);
		if (dogs.length > 0) {
			const dogCollectionIdentifiers = dogCollection.map(dogItem => getDogIdentifier(dogItem));
			const dogsToAdd = dogs.filter(dogItem => {
				const dogIdentifier = getDogIdentifier(dogItem);
				if (dogIdentifier == null || dogCollectionIdentifiers.includes(dogIdentifier)) {
					return false;
				}
				dogCollectionIdentifiers.push(dogIdentifier);
				return true;
			});
			return [...dogsToAdd, ...dogCollection];
		}
		return dogCollection;
	}
};