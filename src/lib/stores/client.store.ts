import type { IClient } from '../domain/Client';
import { getClientIdentifier } from '../domain/Client';
import requestServer from '../utils/requestServer';
import { isPresent } from '../utils/operators';

const resourceUrl = 'http://localhost:8080/api/clients';


export const clientStore = {
	create(client: IClient): Promise<IClient> {
		return requestServer(resourceUrl, { body: JSON.stringify(client), method: 'POST' })
			.then(response => response.json());
	},
	update(client: IClient): Promise<IClient> {
		return requestServer(`${resourceUrl}/${getClientIdentifier(client)}`, {
			body: JSON.stringify(client),
			method: 'PUT'
		})
			.then(response => response.json());
	},
	partialUpdate(client: IClient): Promise<IClient> {
		return requestServer(`${resourceUrl}/${getClientIdentifier(client)}`, {
			body: JSON.stringify(client),
			method: 'PATCH'
		})
			.then(response => response.json());
	},
	find(id: number): Promise<IClient> {
		return requestServer(`${resourceUrl}/${id}`)
			.then(response => response.json());
	},
	query(/*req?: any*/): Promise<IClient[]> {
		//const options = createRequestOption(req);
		return requestServer(resourceUrl)
			.then(response => response.json());
		//, { params: options, observe: 'response' });
	},
	deleteClient(id: number): Promise<Response> {
		return requestServer(`${resourceUrl}/${id}`, { method: 'DELETE' });
	},
	addClientToCollectionIfMissing(clientCollection: IClient[], ...clientsToCheck: (IClient | null | undefined)[]): IClient[] {
		const clients: IClient[] = clientsToCheck.filter(isPresent);
		if (clients.length > 0) {
			const clientCollectionIdentifiers = clientCollection.map(clientItem => getClientIdentifier(clientItem));
			const clientsToAdd = clients.filter(clientItem => {
				const clientIdentifier = getClientIdentifier(clientItem);
				if (clientIdentifier == null || clientCollectionIdentifiers.includes(clientIdentifier)) {
					return false;
				}
				clientCollectionIdentifiers.push(clientIdentifier);
				return true;
			});
			return [...clientsToAdd, ...clientCollection];
		}
		return clientCollection;
	}
};
