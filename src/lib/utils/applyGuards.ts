import {accountStore} from '../stores/account.store';
import type {Account} from '../domain/Account';
import {error} from "@sveltejs/kit";

let currentAccount: Account = null;
accountStore.subscribe(account => currentAccount = account);


export default function applyGuards(guardConfiguration: GuardConfiguration = {}) {
    console.log(currentAccount);
    const authorities = guardConfiguration.authorities;
    if (authorities && authorities.length > 0) {
        if (!accountStore.hasAnyAuthority(authorities)) {
            throw error(403, 'You are not authorized to acess this page');
        }
    }

    return {};
}

export interface GuardConfiguration {
    authorities?: string[];
}