import { authServerProvider } from '../stores/auth-server.store';

export default function requestServer(url: string, params: RequestInit = {}): Promise<Response> {
	const paramsEnriched: RequestInit = {};
	paramsEnriched.headers = {
		'Content-Type': 'application/json'
	};
	if (authServerProvider.getToken()) {
		paramsEnriched.headers.Authorization = `Bearer ${authServerProvider.getToken()}`;
	}
	return fetch(url, { ...paramsEnriched, ...params });
}
