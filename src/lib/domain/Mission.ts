//import * as dayjs from 'dayjs';
import type { IPlace } from './Place';
import type { IClient } from './Client';
import type { IDog } from './Dog';
import type dayjs from 'dayjs';

export interface IMission {
	id?: number;
	name?: string | null;
	date?: dayjs.Dayjs;
	description?: string | null;
	missionLocation?: IPlace | null;
	victims?: IClient[] | null;
	saviors?: IDog[];
}

export class Mission implements IMission {
	constructor(
		public id?: number,
		public name?: string | null,
		public date?: dayjs.Dayjs,
		public description?: string | null,
		public missionLocation?: IPlace | null,
		public victims?: IClient[] | null,
		public saviors?: IDog[]
	) {
	}
}

export function getMissionIdentifier(mission: IMission): number | undefined {
	return mission.id;
}
