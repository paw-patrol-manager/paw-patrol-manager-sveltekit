import type { IDog } from './Dog';

export interface IVehicle {
  id?: number;
  name?: string;
  description?: string | null;
  owner?: IDog | null;
}

export class Vehicle implements IVehicle {
  constructor(public id?: number, public name?: string, public description?: string | null, public owner?: IDog | null) {}
}

export function getVehicleIdentifier(vehicle: IVehicle): number | undefined {
  return vehicle.id;
}
