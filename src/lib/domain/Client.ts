import type { IMission } from './Mission';

export interface IClient {
	id?: number;
	name?: string;
	species?: string;
	occupation?: string | null;
	description?: string | null;
	savings?: IMission[] | null;
}

export class Client implements IClient {
	constructor(
		public id?: number,
		public name?: string,
		public species?: string,
		public occupation?: string | null,
		public description?: string | null,
		public savings?: IMission[] | null
	) {
	}
}

export function getClientIdentifier(client: IClient): number | undefined {
	return client.id;
}
