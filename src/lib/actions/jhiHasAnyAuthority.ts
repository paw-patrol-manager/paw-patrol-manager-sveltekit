import { accountStore } from '../stores/account.store';

export default function jhiHasAnyAuthority(node: HTMLElement, wantedAuthority: string): { update?: () => void, destroy?: () => void } {
	accountStore.subscribe(account => {
		if (account?.authorities?.includes(wantedAuthority)) {
			node.style.display = null;
		} else {
			node.style.display = 'none';
		}
	});
	node.style.fontWeight = 'bolder';
	return {
		update() {
			console.log('Updated');
		},
		destroy() {
			console.debug('Destroyed');
		}
	};
}
