import applyGuards from "$lib/utils/applyGuards";

export function load() {
    return applyGuards({authorities: ['ROLE_ADMIN']});
}
