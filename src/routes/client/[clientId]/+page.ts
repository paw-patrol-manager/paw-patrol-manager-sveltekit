import type {PageData, PageLoad} from "./$types";
import { error } from "@sveltejs/kit";

export const load = (({params:{clientId}}): PageData => {
    if (clientId === "404") {
        throw error(500, "Voluntary error");
    }
    return {
        clientIdentifier: clientId,
    };
})satisfies PageLoad